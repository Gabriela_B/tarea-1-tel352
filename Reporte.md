---
title: "Tarea 1: Algoritmos de Búsqueda en Pacman"
author: "Gabriela Bustamante"
date: "12/10/2021"
---
# Tarea 1: Algoritmos de Búsqueda en Pacman

enlace video explicativo: https://drive.google.com/file/d/1I4gJYUatwP08ptZrvgY3b629tgdw0xZp/view?usp=sharing

En el siguiente documento expondre las modificacion efectuadas al problema original y la comparacion de los resultados respecto al original.

En esta ocasion elegí cambiar el Ambiente. Esto lo logré creando dos nuevos mapas para que se desenvolviera el agente. El primer mapa esta completamente vacio y el agente y su objetivo estan en direcciones opuestas; esto fue porque queria ver de manera mas grafica como se desenvolvia el agente dentro del laberinto. El segundo laberinto lo hice con formas distintas a las que aparecian a los otros laberintos para averiguar si esto representaba una mayor dificultad para el agente.

La creacion de estos laberintos no presento mucha dificultuad ya que solo debí crear un archivo. lay, pero si mucho tiempo ya que se debieron de tipear a mano los caracteres de este. Sin embargo, en temas de codigo solo debí cambiar una linea en _searchAgents.py (línea 147) _, la cual indicaba la posicion incial del objetivo dentro del laberinto (para el laberinto 1 no debí cambiar nada ya que no movi el objetivo de su lugar).

Antes de sacar mis conclusiones primero debemos analizar los resultados obtenidos con el problema original. Para comparar los 3 algoritmos escogí el laberinto más grande y los ejecuté, de estas ejecuciones pude concluir que el algoritmo más optimo para este ambiente era el Depth first Search, ya que encontró el camino mas corto utilizando menos memoria. Esto me sorprendió ya que yo creí que el más optimo sería el A*, sim embargo, este resulto ser solo un poco mejor que BFS ya que exploro unos cuantos nodos menos pero igualmente fue el que tardo más tiempo de los tres.

Ya con estas conclusiones sobre el laberinto original pase a analizar los otros dos laberintos. En el primer laberinto el algoritmo mas optimo fue BFS, ya que tuvo un menor costo en menos tiempo, pero lo siguió de cerca A* ya que tuvo el mismo costo, pero este utilizo mas tiempo y al final quedó DFS con un costo casi 12 veces mayor que los otros dos, sin embargo, utilizo un tiempo similar a BFS. Para el segundo laberinto fue mas fácil escoger el algoritmo más optimo ya que, por un lado, DFS se expandió menos sus nodos y por el otro, A* tuvo un menor costo, pero tuvo que usar unos cuantos nodos más para expandirse, no obstante, decidi priorizar un menor costo o un menor numero de nodos expandidos, por lo cual elegí al algoritmo A* como el más optimo.

A modo de conclusion, podemos decir que no existe un algoritmo que sea optimo en todos los casos porque siempre hay diversos factores que influyen en el desarrollo del problema.

