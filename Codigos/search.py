# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

from game import Actions
import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def expand(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (child,
        action, stepCost), where 'child' is a child to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that child.
        """
        util.raiseNotDefined()

    def getActions(self, state):
        """
          state: Search state

        For a given state, this should return a list of possible actions.
        """
            
        util.raiseNotDefined()

    def getActionCost(self, state, action, next_state):
        """
          state: Search state
          action: action taken at state.
          next_state: next Search state after taking action.

        For a given state, this should return the cost of the (s, a, s') transition.
        """
        util.raiseNotDefined()

    def getNextState(self, state, action):
        """
          state: Search state
          action: action taken at state

        For a given state, this should return the next state after taking action from state.
        """
        util.raiseNotDefined()

    def getCostOfActionSequence(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print("Start:", problem.getStartState())
    print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
    """
    "*** YOUR CODE HERE ***"
    #La estructura utilizada es STACK
    frontier = util.Stack()
    #Lista que contendra los estados explorados
    expanded = []
    #Definimos el estado inicial
    startState = problem.getStartState()
    #definimos un elemento que contiene el estado inicial y una lista para registar el camino a seguir
    start = (startState, [])
    frontier.push(start)

    while frontier.isEmpty() == False:
        #Comienza a explorar el último nodo, el más reciente en la frontera
        state, Actions = frontier.pop()

        if problem.isGoalState(state):
                return Actions

        if state not in expanded:
            #agregamos el estado actual a los explorados
            expanded.append(state)
            
            #Creamos una tupla de posibles nodos sucesores de forma: (child, action, stepCost)
            Sucesor = problem.expand(state)

            #hace un push de cada sucesor a la frontera
            for elemento in Sucesor:
                nextAction = Actions + [elemento[1]] #se le añade la nueva indicacion
                newNodo = (elemento[0], nextAction) 
                frontier.push(newNodo) #agregamos el nuevo nodo a la frontera que contiene la eleccion del child y la accion a tomar
    return Actions

    #util.raiseNotDefined()

def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"
    #importamos la clase Queue
    from util import Queue

    queueXY = Queue()
    visited = [] #estados visitados
    camino = [] #Cada estado mantiene su camino  desde el estado inicial

    # Revisar si el estado es el deseado #
    if problem.isGoalState(problem.getStartState()):
        return []

    # Empieza desde el comienzo y encuentra la solución , el camino  es una lista vacia #
    queueXY.push((problem.getStartState(),[]))

    while(True):

        # Terminamos nuestra condicion: No encontramos una solución #
        if queueXY.isEmpty():
            return []

        # Obtenemos la información sobre su estado actual  #
        xy,camino = queueXY.pop() # Obtenemos la posición y el camino 
        visited.append(xy)

        # Termina la condicion: se alcanzó el objetivo  #
        if problem.isGoalState(xy):
            return camino

        # obtenemos los sucesores al estado actual   #
        suc = problem.expand(xy)


        # Añadimos nuevos estados a la cola y arreglamos su camino #
        if suc :
            for item in suc :
                if item[0] not in visited and item[0] not in (state[0] for state in queueXY.list):

                    nuevoCamino = camino + [item[1]] # Calculamos el camino 
                    queueXY.push((item[0],nuevoCamino))

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    """Busca el nodo que tiene el menor costo combinado y la primera Heuristica ."""

    #tomamos un objeto, costo + heuristica 
    frontier = util.PriorityQueue()

    exploredNodes = [] #guardamos (estado, costo)

    startState = problem.getStartState()
    startNode = (startState, [], 0) #(estado, accion, costo)

    frontier.push(startNode, 0)

    while not frontier.isEmpty():

        #Comenzamos a explorar el primer (menor combinado (costo + heuristica))  nodo en frontera 
        currentState, actions, currentCost = frontier.pop()

        #poner el nodo emergente en la lista explorad
        currentNode = (currentState, currentCost)
        exploredNodes.append((currentState, currentCost))

        if problem.isGoalState(currentState):
            return actions

        else:
            #con la lista de (sucesor, acción, costo)
            successors = problem.expand(currentState)

            #examinamos cada sucesor
            for succState, succAction, succCost in successors:
                newAction = actions + [succAction]
                newCost = problem.getCostOfActionSequence(newAction)
                newNode = (succState, newAction, newCost)

                #Revisamos si el sucedor ha sido explorad
                already_explored = False
                for explored in exploredNodes:
                    #examinamos cada tupla de nodos explorada
                    exploredState, exploredCost = explored

                    if (succState == exploredState) and (newCost >= exploredCost):
                        already_explored = True

                #Si el sucesor no ha sido explorado, ponemos en frontera y lista explorad
                if not already_explored:
                    frontier.push(newNode, newCost + heuristic(succState, problem))
                    exploredNodes.append((succState, newCost))

    return actions

    
    util.raiseNotDefined()

# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
